import React from "react";


const BookCard = props => {
  const book = props.books.map(item =>(
    <div className="book-card" key={item.title}>
      <img className="img-book" src= { item.image } alt={ item.image } />
      <h4 className ="title-book">{item.title}</h4>
      <p className ="author--book" >{item.author}</p>
      <h5 clasName ="price-book">{ item.price }</h5>
    </div>
)
)
  return(
    <div className="book-container">
      { book }
    </div>
  )
}

export default BookCard;

