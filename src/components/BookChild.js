import React, { Component } from "react";
import BookCard from './BookCard';

class BookChild extends Component{
  state ={
    books:[
      {
        title :"Buku Aktivitas Anak Shaleh : Kegiatan",
        author : "Yeti Nurmayanti",
        price : "Rp. 57.000",
        image : require ("../styles/img/1.jpg")
      },
      {
        title :"Komik Kkpk : Festival Unicorn",
        author : "Naura Nuraqila, Dkk",
        price : "Rp. 35.000",
        image : require ("../styles/img/2.jpg")
      },
      {
        title : "Tanya Jawab Seru Tentang Lautan",
        author : "Miles Kelly",
        price : "Rp. 45.000",
        image : require ("../styles/img/3.jpg")
      },
      {
        title :"Komik Next G Jujur Itu Penting",
        author : "Wyda Khosyighina Maitsa, dkk.",
        price : "Rp. 36.000",
        image : require ("../styles/img/4.jpg")
      },
      {
        title :"Tanya Jawab Seru Tentang Tubuh",
        author : "Miles Kelly",
        price : "Rp. 45.000",
        image : require ("../styles/img/5.jpg")
      },
      {
        title :"Ensiklopedia 4D: Tubuh Manusia",
        author : "Devar Entertainment",
        price : "Rp. 82.000",
        image : require ("../styles/img/6.jpg")
      },
      {
        title : "Educomics : Keluarga Super Irit 33: Cara ",
        author : "STORY BOX",
        price : "Rp. 105.000",
        image : require ("../styles/img/7.jpg")
      },
      {
        title :"Komik Kkpk : Hacker Cilik",
        author : "Abdurrahman Aufa Liamrillah, dkk.",
        price : "Rp. 35.000",
        image : require ("../styles/img/8.jpg")
      },
      {
        title :"Ensiklopedia Anak Hebat : Hewan (2019)",
        author : "IROOM",
        price : "Rp. 135.000",
        image : require ("../styles/img/9.jpg")
      },
      {
        title :"365 Aktivitas Seru Sepanjang Tahun",
        author : "Ballon Media",
        price : "Rp. 140.000",
        image : require ("../styles/img/10.jpg")
      },
      {
        title : "Jangan Panik 4",
        author : "Watiek Ideo & Fitri Kurniawan",
        price : "Rp. 84.000",
        image : require ("../styles/img/11.jpg")
      },
      {
        title :"100 Hal yang Tidak Kamu Ketahui - Ruang ",
        author : "Alex Frith",
        price : "Rp. 95.000",
        image : require ("../styles/img/12.jpg")
      }      
    ]
  }

  render(){
    return(
      <div>
        <BookChild className="books" books= { this.state.books }/>
      </div>
    )
  }
}

export default BookChild;
