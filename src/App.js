import React from 'react';
import Title from './components/Title.js'
import BookChild from './components/BookChild.js'

class App extends React.Component{
  render(){
    return(
      <div>
        <Title/>
        <BookChild/>
     </div>
    )
  }
}



export default App;
